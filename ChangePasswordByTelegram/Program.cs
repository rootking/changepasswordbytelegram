﻿using System;
using System.Linq;
using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;
using Telegram.Bot.Types.InlineQueryResults;
using Telegram.Bot.Types.ReplyMarkups;
using MihaZupan;
using System.DirectoryServices;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using System.Net.Http;


namespace ChangePasswordByTelegram
{
    class Program
    {

        private static TelegramBotClient Bot;
        private static string LDAPServer = Properties.Settings.Default.LDAPServer;
        private static string AdminUsername = Properties.Settings.Default.AdminUsername;
        private static string AdminPassword = Properties.Settings.Default.AdminPassword;
        private static string ou = Properties.Settings.Default.ou;
        private static StringCollection TelegramIDs = Properties.Settings.Default.TelegramIDs;
        private static bool UseProxy = Properties.Settings.Default.UseProxy;
        private static string ProxyServer = Properties.Settings.Default.ProxyServer;
        private static int ProxyPort = Properties.Settings.Default.ProxyPort;
        private static string ProxyUser = Properties.Settings.Default.ProxyUser;
        private static string ProxyPassword = Properties.Settings.Default.ProxyPassword;
        private static string BotKey = Properties.Settings.Default.BotKey;

        public static void Main(string[] args)
        {
            string[] argsLocal = args;
            try
            {
                if (UseProxy)
                {
                    var proxy = new HttpToSocks5Proxy(ProxyServer, ProxyPort, ProxyUser, ProxyPassword);
                    proxy.ResolveHostnamesLocally = true;
                    Bot = new TelegramBotClient(BotKey, proxy);
                }
                else
                {
                    Bot = new TelegramBotClient(BotKey);
                }
                var me = Bot.GetMeAsync().Result;

                Console.Title = me.Username;

                Bot.OnMessage += BotOnMessageReceived;
                Bot.OnMessageEdited += BotOnMessageReceived;
                Bot.OnCallbackQuery += BotOnCallbackQueryReceived;
                Bot.OnInlineQuery += BotOnInlineQueryReceived;
                Bot.OnInlineResultChosen += BotOnChosenInlineResultReceived;
                Bot.OnReceiveError += BotOnReceiveError;

                Bot.StartReceiving(Array.Empty<UpdateType>());
                Console.WriteLine($"Start listening for @{me.Username}");
                while (true) { }
            }
            catch (Exception ex)
            {
               Console.WriteLine(ex.Message);

               Main(argsLocal);
            }

        }
        static string generatePassword()
        {

            Random rnd = new Random();
            string password = "";
            string[] mask = { "1", "2", "3", "4", "5", "6", "7", "8", "9",
                "A", "B", "C", "D", "E","F", "G", "H", "J", "K", "M", "N", "P", "Q", "R", "S", "T", "Y", "U", "V", "W", "X", "Z",
                "a", "b", "c", "d", "e","f", "g", "h", "j", "k", "m", "n", "p", "q", "r", "s", "t", "y", "u", "v", "w", "x", "z"};
            for (int i = 0; i < 8; i++)
            {
                password += mask[rnd.Next(0, mask.Length - 1)];
            }
            Regex checkPassword = new Regex(@"^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$");
            if (checkPassword.IsMatch(password))
                return password;
            else
                return generatePassword();
        }
        private static string resetPassword(string userName, string password)
        {
            try
            {
                DirectoryEntry uEntry = new DirectoryEntry(LDAPServer, AdminUsername, AdminPassword);
                DirectorySearcher search = new DirectorySearcher(uEntry);
                search.Filter = string.Format("(&(objectcategory=user)({0}={1}))", "sAMAccountName", userName);
                var user = search.FindOne();
                uEntry.Path = user.Path;
                if (uEntry.Parent.Name == "OU=" + ou)
                {
                    uEntry.Invoke("SetPassword", new object[] { password });
                }
                uEntry.Properties["lockoutTime"].Value = 0; //unlock account
                uEntry.CommitChanges();
                uEntry.Close();
                return null;
            }
            catch (Exception e) {
                Console.WriteLine(e.Message);
                return e.Message;
            }
        }

        private static async void BotOnMessageReceived(object sender, MessageEventArgs messageEventArgs)
        {
            var message = messageEventArgs.Message;

            Console.WriteLine("Date: " + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString() + " ID: " + message.From.Id.ToString() + " message: " + message.Text);

            if (message == null || message.Type != MessageType.Text || TelegramIDs.IndexOf(message.From.Id.ToString()) == -1) return;

            switch (message.Text.Split(' ').First())
            {
                // send inline keyboard
                //case "/inline":
                //    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.Typing);

                //    await Task.Delay(500); // simulate longer running task

                //    var inlineKeyboard = new InlineKeyboardMarkup(new[]
                //    {
                //        new [] // first row
                //        {
                //            InlineKeyboardButton.WithCallbackData("1.1"),
                //            InlineKeyboardButton.WithCallbackData("1.2"),
                //        },
                //        new [] // second row
                //        {
                //            InlineKeyboardButton.WithCallbackData("2.1"),
                //            InlineKeyboardButton.WithCallbackData("2.2"),
                //        }
                //    });

                //    await Bot.SendTextMessageAsync(
                //        message.Chat.Id,
                //        "Choose",
                //        replyMarkup: inlineKeyboard);
                //    break;

                // send custom keyboard
                //case "/keyboard":
                //    ReplyKeyboardMarkup ReplyKeyboard = new[]
                //    {
                //        new[] { "1.1", "1.2" },
                //        new[] { "2.1", "2.2" },
                //    };

                //    await Bot.SendTextMessageAsync(
                //        message.Chat.Id,
                //        "Choose",
                //        replyMarkup: ReplyKeyboard);
                //    break;

                // send a photo
                //case "/photo":
                //    await Bot.SendChatActionAsync(message.Chat.Id, ChatAction.UploadPhoto);

                //    const string file = @"Files/tux.png";

                //    var fileName = file.Split(Path.DirectorySeparatorChar).Last();

                //    using (var fileStream = new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.Read))
                //    {
                //        await Bot.SendPhotoAsync(
                //            message.Chat.Id,
                //            fileStream,
                //            "Nice Picture");
                //    }
                //    break;

                // request location or contact
                //case "/request":
                //    var RequestReplyKeyboard = new ReplyKeyboardMarkup(new[]
                //    {
                //        KeyboardButton.WithRequestLocation("Location"),
                //        KeyboardButton.WithRequestContact("Contact"),
                //    });

                //    await Bot.SendTextMessageAsync(
                //        message.Chat.Id,
                //        "Who or Where are you?",
                //        replyMarkup: RequestReplyKeyboard);
                //    break;

                case "/start":
                    string usage = @"
Usage:
/reset %USERNAME%
Your ID is " + message.From.Id;
                    await Bot.SendTextMessageAsync(
                            message.Chat.Id,
                            usage,
                            replyMarkup: new ReplyKeyboardRemove());
                    break;
                case "/reset":
                    string password = generatePassword();
                    string username = message.Text.Split(' ').Last();
                    string errorMessage = resetPassword(username,password);
                    if (String.IsNullOrEmpty(errorMessage))
                    {
                        await Bot.SendTextMessageAsync(
                            message.Chat.Id,
                            password);
                        Console.WriteLine(password);
                    } else
                    {
                        await Bot.SendTextMessageAsync(
                            message.Chat.Id,
                            errorMessage);
                    }
                    break;
                    //default:

                    // inline - send inline keyboard
                    // keyboard - send custom keyboard
                    // photo - send a photo
                    // request - request location or contact


            }
        }

        private static async void BotOnCallbackQueryReceived(object sender, CallbackQueryEventArgs callbackQueryEventArgs)
        {
            var callbackQuery = callbackQueryEventArgs.CallbackQuery;

            await Bot.AnswerCallbackQueryAsync(
                callbackQuery.Id,
                $"Received {callbackQuery.Data}");

            await Bot.SendTextMessageAsync(
                callbackQuery.Message.Chat.Id,
                $"Received {callbackQuery.Data}");
        }

        private static async void BotOnInlineQueryReceived(object sender, InlineQueryEventArgs inlineQueryEventArgs)
        {
            Console.WriteLine($"Received inline query from: {inlineQueryEventArgs.InlineQuery.From.Id}");

            InlineQueryResultBase[] results = {
                new InlineQueryResultLocation(
                    id: "1",
                    latitude: 40.7058316f,
                    longitude: -74.2581888f,
                    title: "New York")   // displayed result
                    {
                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 40.7058316f,
                            longitude: -74.2581888f)    // message if result is selected
                    },

                new InlineQueryResultLocation(
                    id: "2",
                    latitude: 13.1449577f,
                    longitude: 52.507629f,
                    title: "Berlin") // displayed result
                    {

                        InputMessageContent = new InputLocationMessageContent(
                            latitude: 13.1449577f,
                            longitude: 52.507629f)   // message if result is selected
                    }
            };

            await Bot.AnswerInlineQueryAsync(
                inlineQueryEventArgs.InlineQuery.Id,
                results,
                isPersonal: true,
                cacheTime: 0);
        }

        private static void BotOnChosenInlineResultReceived(object sender, ChosenInlineResultEventArgs chosenInlineResultEventArgs)
        {
            Console.WriteLine($"Received inline result: {chosenInlineResultEventArgs.ChosenInlineResult.ResultId}");
        }

        private static void BotOnReceiveError(object sender, ReceiveErrorEventArgs receiveErrorEventArgs)
        {
            Console.WriteLine("Received error: {0} — {1}",
                receiveErrorEventArgs.ApiRequestException.ErrorCode,
                receiveErrorEventArgs.ApiRequestException.Message);
        }
    }
}
